import datetime
import json
import random
import time
from threading import Timer
import rossiFaceRecognition
import paho.mqtt.client as mqtt

piface = True

if piface:
    import pifacedigitalio

HOST = 'localhost'
PORT = 1883
mqtt_client = mqtt.Client()

stateUndefined = 0
stateAskedForHeating = 1
stateTurnMachineOff = 2
stateTurnMachineOn = 3
statePositive = 4
stateNegative = 5
stateStoppedListening = 6
stateAskedForCupSize = 7
programState = stateUndefined

sessionToStop = -1
sessionToResetNotificationSound = -1

lastSessionId = 0

stateSenseoOff = 0
stateSenseoHeating = 1
stateSenseoHeated = 2
stateSenseoMakingCoffee = 3
stateSenseoWaterEmpty = 4
senseoState = stateSenseoOff

senseoLastState = stateSenseoOff

smallCup = 0
bigCup = 1

cup_size_face_recognition = -1
face_recognition_asked = False

CUP_SIZE = "Hättest du gerne eine große oder eine kleine Tasse?"
OKAY = "Okay!"
DIDNT_UNDERSTAND = "Ich habe dich leider nicht verstanden."
HEATING_UP = "Heize auf, bitte warte noch einen Moment."
PREHEAT_MACHINE = "Soll ich vorheizen?"
PREHEAT_MACHINE_YES_NOTIFICATION = "Ich sag dir bescheid, wenn ich soweit bin."
PREHEAT_MACHINE_NO_NOTIFICATION = "Dann halt nicht!"
TURN_ON_FIRST = "Die Maschine ist aus bitte mache sie vorher an."
WATER_EMPTY = "Sorrie Wasser ist leer, bitte fülle nach!"
MAKING_COFFEE = "Bitte warte!"
GREETING = "Hai {}, wie immer eine {} Tasse?"
MACHINE_READY = "Bescheid!"
TURNING_MACHINE_OFF = "Ich schalte mich ab"
HEATED_UP = "Bin bereit!"
LOOKING_AT_YOU = "Mal schauen, ob ich dich kenne."
MAKING_BIG_CUP = "Ich mache dir eine große Tasse."
MAKING_SMALL_CUP = "Ich mache dir eine kleine Tasse."


def on_connect(client, userdata, flags, rc):
    print('Connected')
    mqtt_client.subscribe('#')
    set_notifications_sound_state(True)


def start_listening(question, session_id=0):
    print("Start listening")
    global sessionToResetNotificationSound
    set_notifications_sound_state(True)
    if session_id == 0:
        payload = '{"init": {"type": "action", "text": "' + question + '"}, "siteId": "default"}'
        mqtt_client.publish('hermes/dialogueManager/startSession', payload=payload)
    else:
        payload = '{"sessionId": "' + str(session_id) + '", "text": "' + question + '"}'
        mqtt_client.publish('hermes/dialogueManager/continueSession', payload=payload)
    sessionToResetNotificationSound = 1


def set_hotword_status(active):
    payload = '{"siteId": "default"}'
    if active:
        print("Enabling hotword")
        mqtt_client.publish('hermes/hotword/toggleOn', payload=payload)
    else:
        print("Disabling hotword")
        mqtt_client.publish('hermes/hotword/toggleOff', payload=payload)


def stop_listening(session):
    print("Stop listening")
    payload = '{"sessionId": "' + str(session) + '"}'
    print(payload)
    mqtt_client.publish('hermes/dialogueManager/endSession', payload=payload)


def say(text):
    payload = '{"init": {"type": "notification", "text": "' + text + '"}}'
    mqtt_client.publish('hermes/dialogueManager/startSession', payload=payload)


def play_wav(file_name):
    f = open(file_name, "rb")
    bytes_file = bytearray(f.read())
    mqtt_client.publish('hermes/audioServer/default/playBytes/fshfsaliue', bytes_file)


def set_notifications_sound_state(activate):
    payload = '{"siteId": "default"}'
    # if activate:
    print("Enabling notification sound")
    mqtt_client.publish('hermes/feedback/sound/toggleOn', payload=payload)
    # else:
    #     print("Disabling notification sound")
    #     mqtt_client.publish('hermes/feedback/sound/toggleOff', payload=payload)


def turn_machine_on():
    print("Turning machine on!")
    play_wav("Microsoft_Windows_XP_Startup.wav")
    set_senseo_state(stateSenseoHeating)
    if piface:
        pifacedigitalio.digital_write(0, 1)
        time.sleep(1)
        pifacedigitalio.digital_write(0, 0)
    else:
        global off, heating
        off = False
        heating = True


def turn_machine_off():
    print("Turning machine off!")
    play_wav("Microsoft_Windows_XP_Shutdown.wav")
    set_senseo_state(stateSenseoOff)
    if piface:
        pifacedigitalio.digital_write(0, 0)
        time.sleep(1)
        pifacedigitalio.digital_write(0, 1)


def make_cup(size):
    if size == smallCup:
        print("Making small cup")
        if piface:
            say(MAKING_SMALL_CUP)
            pifacedigitalio.digital_write(1, 1)
            time.sleep(1)
            pifacedigitalio.digital_write(1, 0)
    elif size == bigCup:
        print("Making big cup")
        if piface:
            say(MAKING_BIG_CUP)
            pifacedigitalio.digital_write(2, 1)
            time.sleep(1)
            pifacedigitalio.digital_write(2, 0)
    set_senseo_state(stateSenseoMakingCoffee)


def machine_heated():
    result = rossiFaceRecognition.whoIsThisFace()
    if result["name"] == "Unknown":
        print("Unkown")
        start_listening(CUP_SIZE)
    else:
        name = result["name"]
        cup_size = result["cup"]
        print(name + " " + cup_size)
        start_listening(GREETING.format(name, cup_size + "e"))
        global cup_size_face_recognition, face_recognition_asked
        face_recognition_asked = True
        if cup_size == "groß":
            cup_size_face_recognition = bigCup
        elif cup_size == "klein":
            cup_size_face_recognition = smallCup
        elif cup_size == "egal":
            if random.randint(0, 1) == 0:
                cup_size_face_recognition = bigCup
            else:
                cup_size_face_recognition = smallCup

    global programState
    programState = stateAskedForCupSize


def machine_water_empty():
    say(WATER_EMPTY)


isHotwordAlreadyDetected = False
wasLastSayingReady = False


def reset_hotword_state():
    global isHotwordAlreadyDetected
    set_hotword_status(True)
    isHotwordAlreadyDetected = False


# Process a message as it arrives
def on_message(client, userdata, msg):
    try:
        global programState
        global sessionToStop
        global sessionToResetNotificationSound
        global lastSessionId
        global cup_size_face_recognition
        global wasLastSayingReady
        global isHotwordAlreadyDetected
        if msg.topic != 'hermes/audioServer/default/audioFrame':
            now = str(datetime.datetime.now())
            print('{} - {}'.format(now, msg.topic))

        if msg.topic == 'hermes/hotword/default/detected':
            if isHotwordAlreadyDetected:
                print("Skipping this hotword")
                return

            # TODO: Handle water empty in all cases
            print("Reacting to this hotword")
            print(isHotwordAlreadyDetected)
            isHotwordAlreadyDetected = True
            set_hotword_status(False)
            stop_listening(lastSessionId)
            if senseoState == stateSenseoHeated:
                programState = stateAskedForCupSize
                sessionToStop = 0
                say(LOOKING_AT_YOU)
                wasLastSayingReady = True
            elif senseoState == stateSenseoHeating:
                programState = stateAskedForCupSize
                sessionToStop = 0
                say(HEATING_UP)
            elif senseoState == stateSenseoOff:
                programState = stateAskedForHeating
                sessionToStop = 0
                start_listening(PREHEAT_MACHINE)
            elif senseoState == stateSenseoMakingCoffee:
                sessionToStop = 0
                start_listening(MAKING_COFFEE)
            Timer(0.5, reset_hotword_state).start()

        elif msg.topic == 'hermes/dialogueManager/sessionStarted':
            stop_session_if_needed(msg)

        elif msg.topic == 'hermes/dialogueManager/sessionQueued' and sessionToStop >= 0:
            stop_session_if_needed(msg)

        elif msg.topic == 'hermes/dialogueManager/sessionEnded' and sessionToResetNotificationSound >= 0:
            if sessionToResetNotificationSound == 0:
                set_notifications_sound_state(False)

            reset_hotword_state()
            sessionToResetNotificationSound -= 1

        elif msg.topic == 'hermes/intent/supermar1010:MakeCup':
            handle_make_cup(lastSessionId, msg)

        elif msg.topic == 'hermes/intent/supermar1010:TurnMachineOff':
            programState = stateTurnMachineOff
            turn_machine_off()
            say(OKAY)

        elif msg.topic == 'hermes/intent/supermar1010:TurnMachineOn':
            programState = stateTurnMachineOn
            turn_machine_on()
            say(OKAY)

        elif msg.topic == 'hermes/intent/supermar1010:Negative':
            if programState == stateAskedForHeating:
                stop_listening(lastSessionId)
                say(OKAY + " " + PREHEAT_MACHINE_NO_NOTIFICATION)

            global face_recognition_asked
            if not face_recognition_asked:
                face_recognition_asked = False
                say(OKAY + " " + PREHEAT_MACHINE_NO_NOTIFICATION)

            elif programState == stateAskedForCupSize:
                face_recognition_asked = False
                start_listening(CUP_SIZE)

            programState = stateNegative

        elif msg.topic == 'hermes/intent/supermar1010:Positive':
            if programState == stateAskedForHeating:
                stop_listening(lastSessionId)
                say(OKAY + " " + PREHEAT_MACHINE_YES_NOTIFICATION)
                turn_machine_on()

            if programState == stateAskedForCupSize:
                make_cup(cup_size_face_recognition)
                cup_size_face_recognition = -1

            programState = statePositive

        elif msg.topic == 'hermes/tts/sayFinished':
            if wasLastSayingReady:
                print("machine_heated")
                machine_heated()
                wasLastSayingReady = False

    except Exception as err:
        print(err)


def stop_session_if_needed(msg):
    global lastSessionId, sessionToStop, programState
    payload = msg.payload.decode("utf-8")
    print(payload)
    if sessionToStop == 0:
        print("Stopping session")
        if len(payload) > 0:
            data = json.loads(payload)
            lastSessionId = data.get('sessionId')
            stop_listening(lastSessionId)

        if senseoState == stateSenseoHeated:
            programState = stateAskedForCupSize

    data = json.loads(payload)
    lastSessionId = data.get('sessionId')
    sessionToStop -= 1


def handle_make_cup(last_session_id, msg):
    if senseoState == stateSenseoWaterEmpty:
        say(WATER_EMPTY)
    elif senseoState == stateSenseoOff:
        say(TURN_ON_FIRST)

    payload = msg.payload.decode("utf-8")
    data = json.loads(payload)
    print(payload)
    # Nach cup keine sound
    size = data['slots'][0]['rawValue']
    stop_listening(last_session_id)
    if "groß" in size or "riesig" in size:
        cup_size = bigCup
        make_cup(cup_size)
        # say(OKAY)
    elif "klein" in size or "mini" in size:
        cup_size = smallCup
        make_cup(cup_size)
        # say(OKAY)
    else:
        say(DIDNT_UNDERSTAND)
        start_listening(CUP_SIZE)


if piface:
    pifacedigitalio.init()
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message
mqtt_client.connect(HOST, PORT)

heated = False
heating = False
empty = False
off = False

start_time = time.time()


def is_heated():
    if piface:
        return pifacedigitalio.digital_read(7) == 0
    else:
        return time.time() - start_time > 30


def is_heating():
    if piface:
        return pifacedigitalio.digital_read(6) == 0
    else:
        return 30 > time.time() - start_time > 15


def is_water_empty():
    if piface:
        return pifacedigitalio.digital_read(5) == 0
    else:
        return empty


def is_machine_off():
    if piface:
        return pifacedigitalio.digital_read(7) == 1 and pifacedigitalio.digital_read(
            6) == 1 and pifacedigitalio.digital_read(5) == 1
    else:
        return off


def set_senseo_state(new_state):
    global senseoState, senseoLastState
    senseoLastState = senseoState
    senseoState = new_state


debounce_treshold = 10
machine_off_debounce = 0
machine_heated_debounce = 0
machine_heating_debounce = 0
machine_water_empty_debounce = 0

while True:
    mqtt_client.loop(timeout=0.5)

    if is_machine_off() and not senseoState == stateSenseoOff:
        machine_off_debounce += 1
        machine_water_empty_debounce = 0
        machine_heated_debounce = 0
        machine_heating_debounce = 0
        if machine_off_debounce > debounce_treshold:
            set_senseo_state(stateSenseoOff)

    if is_heating() and not senseoState == stateSenseoHeating:
        machine_heating_debounce += 1
        machine_off_debounce = 0
        machine_water_empty_debounce = 0
        machine_heated_debounce = 0
        if machine_heating_debounce > debounce_treshold:
            set_senseo_state(stateSenseoHeating)

    if is_heated() and not senseoState == stateSenseoHeated:
        machine_heated_debounce += 1
        machine_water_empty_debounce = 0
        machine_off_debounce = 0
        machine_heating_debounce = 0
        if machine_heated_debounce > debounce_treshold:
            set_senseo_state(stateSenseoHeated)
            if not (senseoLastState == stateSenseoHeated or senseoLastState == stateSenseoMakingCoffee):
                say(HEATED_UP)

    if is_water_empty() and not senseoState == stateSenseoWaterEmpty:
        machine_water_empty_debounce += 1
        machine_off_debounce = 0
        machine_heated_debounce = 0
        machine_heating_debounce = 0
        if machine_water_empty_debounce > debounce_treshold:
            set_senseo_state(stateSenseoWaterEmpty)
            machine_water_empty()
