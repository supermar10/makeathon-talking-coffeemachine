# Snips important information

## Random notes

What to do:

- Make assistant online in console.snips.ai
- sam install assistant https://docs.snips.ai/getting-started/quick-start-raspberry-pi
- Intents https://snips.gitbook.io/tutorials/t/technical-guides/listening-to-intents-over-mqtt-using-python
- personal wake word https://github.com/snipsco/snips-record-personal-hotword

## How to

Based on https://docs.snips.ai/getting-started/quick-start-raspberry-pi

### Related to raspberry pi

1. Install npm and node on your pc
2. ```sudo npm install -g snips-sam``` or execute ```npm install -g snips-sam``` in a administrator cmd, I think it should work like this on Windows, not sure
3. ```sam connect raspberrypi``` replace raspberrypi by the IP address of the pi
4. ```sam init```
5. Check if everything works ```sam status```, it should show information about the pi
6. ```sam setup audio``` to setup audio select the appropriate speaker and microphone
7. Test with ```sam test microphone``` and ```sam test speaker```
8. ```sam login``` use the credentials I supplied
9. ```sam install assistant``` This will ask which assistant to install, choose MakeathonDeutsch
10. Now everything should be setup on the Pi side. You can test with ```sam watch``` and say "Hey Snips" this should make a bling sound and trigger some output on the console
11. To make it use your own hotword follow this <https://github.com/snipsco/snips-record-personal-hotword>, optional for testing

### PC

1. Open main.py in your editor/IDE of choice and change the Ip address to whichever your Pi has
2. Run it
3. Profit